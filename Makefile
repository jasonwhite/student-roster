.PHONY: all clean

default: all

all: unittest roster

unittest: roster.d sqlite3.d
	dmd -g -gs -main -unittest -L-lsqlite3 $^ -of$@

roster: main.d roster.d sqlite3.d
	dmd -L-lsqlite3 $^ -of$@

clean:
	$(RM) unittest roster *.o *.db
