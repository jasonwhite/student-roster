# Student Roster

This is a sample program for using an SQLite3 database with the D language. It
maintains a database of students, courses, and the classes students are in. It
can be extended to implement a useful program.

## Dependencies

 * [`dmd >= 2.064`](http://dlang.org/download.html)
 * `libsqlite3-dev`. Install it with `sudo apt-get install libsqlite3-dev`.

## Running It

	$ make unittest
	$ ./unittest

This will compile and run the unit test. If all goes well, nothing will be
displayed.
