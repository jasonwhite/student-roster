/**
  Authors: Jason White

  Description:
  This is the back-end for the roster database.
 */
module roster;

import std.stdio, std.exception, std.datetime, std.string, std.conv;

import sqlite3;


/**
  Bit field of days to meet at.
 */
enum MeetingDays : uint
{
	sunday    = 1 << 0,
	monday    = 1 << 1,
	tuesday   = 1 << 2,
	wednesday = 1 << 3,
	thursday  = 1 << 4,
	friday    = 1 << 5,
	saturday  = 1 << 6,

	// Common meeting days.
	monWed    = monday | wednesday,
	monWedFri = monday | wednesday | friday,
	tuTh      = tuesday | thursday,
}

/**
  An individual course.
 */
struct Course
{
	/// Name of the course (e.g., "CSE431-01").
	string name;

	/// Name of the instructor.
	string instructor;

	/// The time of day interval at which the course is scheduled to meet.
	uint startTime, endTime;

	/// Days of the week to meet on.
	MeetingDays days;

	/// Maximum number of students that can be in the class
	uint maxStudents;
}

/**
  An individual student.
 */
struct Student
{
	// Name of the student.
	string name;

	// Student ID
	uint sid;

	// Other stuff if needed...
}

/**
  A class is a student associated with a course.
 */
struct Class
{
	// Name of the student
	string student;

	// Name of the course
	string course;
}

/**
  A roster holds all students, courses, and classes.

  (Is "roster" even the right name for what this is?)
 */
struct Roster
{
	private SQLite3 db;

	this(string file)
	{
		db = new SQLite3(file);

		// BEGIN/COMMIT reduces disk usage and dramatically speeds things up.
		db.execute("BEGIN");

		// Create tables if they don't already exist.
		enum statements = [
			// Table of courses
			`CREATE TABLE IF NOT EXISTS courses(
				name TEXT PRIMARY KEY,
				instructor TEXT,
				startTime UNSIGNED INTEGER,
				endTime UNSIGNED INTEGER,
				days UNSIGNED INTEGER,
				maxStudents UNSIGNED INTEGER
			);`,

			// Table of students
			`CREATE TABLE IF NOT EXISTS students(
				name TEXT PRIMARY KEY,
				sid UNSIGNED INTEGER UNIQUE
			);`,

			// Table of classes
			// TODO: Use table indexes instead of strings
			`CREATE TABLE IF NOT EXISTS classes(
				student TEXT,
				course TEXT
			);`,
		];

		foreach (s; statements)
			db.execute(s);
	}

	~this()
	{
		db.execute("COMMIT");
		db.close();
	}

	/**
	  Adds a course to the database.
	 */
	void addCourse(in Course course)
	{
		enum sql = `INSERT OR REPLACE INTO courses VALUES(?, ?, ?, ?, ?, ?)`;

		with (course)
			db.execute(sql,
				name,
				instructor,
				startTime,
				endTime,
				days,
				maxStudents
				);
	}

	/**
	  Adds a student to the database.
	 */
	void addStudent(in Student student)
	{
		enum sql = `INSERT OR REPLACE INTO students VALUES(?, ?)`;

		with (student) db.execute(sql, name, sid);
	}

	/**
	  Adds a class to the database.
	 */
	void addClass(in Class c)
	{
		// TODO: Prevent duplicate records from being inserted.
		enum sql = `INSERT OR REPLACE INTO classes VALUES(?, ?)`;

		with (c) db.execute(sql, student, course);
	}

	/**
	  Finds a course by name. If the course is not found, an exception is
	  thrown.
	 */
	Course findCourse(string courseName)
	{
		enum sql = `SELECT * FROM courses WHERE name=?`;
		auto s = db.prepare(sql, courseName);

		enforce(s.step(), "Could not find course.");

		Course c;

		with (c)
			s.getRow(
				name,
				instructor,
				startTime,
				endTime,
				days,
				maxStudents
				);

		return c;
	}

	/**
	  Finds a student by name. If the student is not found, an exception is
	  thrown.
	 */
	Student findStudent(string studentName)
	{
		enum sql = `SELECT * FROM students WHERE name=?`;
		auto s = db.prepare(sql, studentName);

		enforce(s.step(), "Could not find student.");

		Student student;

		with (student) s.getRow(name, sid);

		return student;
	}

	/**
	  Finds all the courses a student is in.
	 */
	string[] findCoursesWithStudent(string studentName)
	{
		enum sql = `SELECT course FROM classes WHERE student=?`;
		auto s = db.prepare(sql, studentName);

		string[] courses;

		while (s.step())
			courses ~= s.get!string(0);

		return courses;
	}

	// Any other useful database queries...
}

unittest
{
	import std.file;

	immutable db = "_deleteme.db";

	scope (exit) remove(db);
	auto roster = Roster(db);

	// Add a course.
	immutable Course cse201 = {
		name: "CSE201",
		instructor: "Dr. Evil",
		startTime: 1200,
		endTime: 1350,
		days: MeetingDays.tuTh,
		maxStudents: 42,
	};

	roster.addCourse(cse201);

	assert(roster.findCourse("CSE201") == cse201);

	// Add some students and put them in a class -- even if they don't want to
	// be there.
	immutable students = [
		"Billy Bob",
		"John Smith",
		"Jane Doe",
		"Robert'); DROP TABLE students;--", // http://xkcd.com/327/
		];

	foreach (i, s; students)
	{
		roster.addStudent(Student(s, cast(uint)i));
		roster.addClass(Class(s,"CSE201"));
	}

	assert(roster.findStudent("Jane Doe") == Student("Jane Doe", 2));

	assert(roster.findCoursesWithStudent("Billy Bob") == ["CSE201"]);
}
